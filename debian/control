Source: libio-termios-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libio-pty-perl <!nocheck>,
                     libtest2-suite-perl <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libio-termios-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libio-termios-perl.git
Homepage: https://metacpan.org/release/IO-Termios
Rules-Requires-Root: no

Package: libio-termios-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libio-pty-perl
Suggests: liblinux-termios2-perl
Description: module to supply termios methods to IO::Handle objects
 The IO::Termios class extends the generic IO::Handle object class by
 providing methods which access the system's terminal control termios(3)
 operations. These methods are primarily of interest when dealing with TTY
 devices, including serial ports.
 .
 Linux supports a non-POSIX extension to the usual termios interface which
 allows arbitrary baud rates to be set. IO::Termios can automatically make use
 of this ability through the Linux::Termios2 module which is already in Debian
 as package liblinux-termios2-perl. This module will be used automatically and
 transparently to allow set*baud methods to set any rate allowed by the
 kernel/driver.
